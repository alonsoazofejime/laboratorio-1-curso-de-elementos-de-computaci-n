#Entrada: Un numero entero
#Salida: la suma de los digitos de ese numero
#Que hace?: suma los digitos de un numero

def suma_digitos(numero):
  lista_de_numero = list(str(numero))
  contador = 0
  for elem in lista_de_numero:
  	x = int(elem)
  	contador += x
  return contador
  
print(suma_digitos(123))