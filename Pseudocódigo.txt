Pseudocódigos del laboratorio #1

1. Una función que elimine un n dígito de un número
	Se ingresa un parámetro y un número
	Definimos una lista vacía
	Combertimos en string el parámetro y el número
	Definimos un contador
	Por medio de un for analizamos la lista del número, para observar si hay un numero igual al parámetro
	Retornamos el contador	

2. Cuantos digitos hay en común en un número
	Ingresa un parámetro y un número
	Definimos una lista vacía
	Combertimos en string el parámetro y el número
	Definimos un contador
	Analizamos cada digito del numero comaprarándolo con el parámetro, y sí encontramos una coincidencia, le sumamos 1 al contador
	Retornamos el contador

3. Función que recibe un número y lo devuelve al revés
 	Entra un número
	Lo transformamos en string
	El string lo voltemaos con [::-1]
	Devolvemos el número

4. Función que recibe un número y que sume cada dígito 
	Entra un número
	Combertimos ese número en lista de strings
	Definimos un contador
	Usando un for___in. para que con cada vuelta aga lo siguiente:
		combierto el elemento en un número entero
		sumo ese número al contador preestablecido
	Ya terminado el for retornamos el contador